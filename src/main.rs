#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate dotenv;
extern crate regex;
extern crate rocket;
extern crate rocket_contrib;
extern crate pulldown_cmark;
#[macro_use] extern crate diesel;
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate serde_derive;

mod db;
mod files;
mod markdown;
mod metrics;
mod static_files;

use metrics::MetricCollector;
use rocket_contrib::Template;

#[derive(Serialize)]
struct EmptyContext {}

#[get("/")]
fn index() -> Template {
    Template::render("index", EmptyContext {})
}

#[get("/gifs")]
fn gifs() -> Template {
    Template::render("gifs", EmptyContext {})
}

fn main() {
    rocket::ignite()
        .attach(Template::fairing())
        .attach(MetricCollector::fairing())
        .manage(db::init_pool())
        .mount("/",
            routes![
                index,
                gifs,
                files::files_view,
                metrics::metrics_view,
                markdown::markdown_view,
                static_files::serve
            ]
        )
        .launch();
}
