use std::path::{Path, PathBuf};
use std::fs::File;
use std::io::prelude::*;

use pulldown_cmark::{Parser, html, Event};
use rocket_contrib::Template;
use regex::Regex;

#[derive(Serialize)]
struct MarkdownContext {
    error: Option<String>,
    markdown: Option<String>
}

#[get("/markdown/<file_path..>")]
fn markdown_view(mut file_path: PathBuf) -> Option<Template> {
    file_path.set_extension("md");
    let mut file = File::open(
        Path::new("markdown").join(file_path)
    ).ok()?;
    let mut contents = String::new();

    let mut context = MarkdownContext {
        error: None,
        markdown: None
    };

    match file.read_to_string(&mut contents) {
        Ok(_) => {
            let mut rendered = String::new();
            safe_markdown(&contents, &mut rendered);
            context.markdown = Some(rendered);
        },
        Err(e) => {
            println!("Failed to read markdown file: {:?}", e);
            context.error = Some(String::from("Failed to read file contents."));
        }
    }

    Some(Template::render("markdown", &context))
}

fn safe_markdown(md: &str, rendered: &mut String) {
    let parser = Parser::new(md)
        .filter(
            |x| match x {
                Event::Html(_) => false,
                Event::InlineHtml(html) => is_safe_inline_html(html),
                _ => true
            });
    html::push_html(rendered, parser);
}

fn is_safe_inline_html(html: &str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r#"^<(img) ((src|width|height|align|alt)="[a-zA-Z0-9_:/. ']*" ?){0,20}/?>$"#).unwrap();
    }

    RE.is_match(html)
}
