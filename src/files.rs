use rocket_contrib::Template;
use std::fs;


#[derive(Serialize)]
struct FilesContext {
    markdown_files: Vec<String>
}

#[get("/files")]
fn files_view() -> Template {
    let paths_option = match fs::read_dir("markdown/") {
        Ok(rdir) => Some(rdir),
        Err(e) => {
            println!("Error reading markdown dir: {:?}", e);
            None
        }
    };

    let mut markdown_files = vec![];
    if let Some(paths) = paths_option {
        for path in paths {
            println!("{:?}", path);
            if let Ok(entry) = path {
                let file_path = entry.path().into_boxed_path();
                if file_path.is_file() {
                    markdown_files.push(
                        file_path.file_stem()
                            .unwrap()
                            .to_string_lossy()
                            .to_string()
                    );
                }
            }
        }
    }
    Template::render("files", FilesContext { markdown_files })
}
