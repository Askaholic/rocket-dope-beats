use diesel;
use diesel::prelude::*;
use diesel::pg::PgConnection;
use diesel::result::Error;

use super::schema::metrics;


#[derive(Queryable, Identifiable, Clone, Serialize)]
pub struct Metric {
    pub id: i32,
    pub uri: String,
    pub view_count: i32
}

#[derive(Insertable)]
#[table_name="metrics"]
pub struct NewMetric {
    pub uri: String,
    pub view_count: i32
}

impl Metric {

    pub fn all(conn: &PgConnection) -> Result<Vec<Metric>, Error> {
        use super::schema::metrics::dsl::*;

        metrics.load::<Metric>(conn)
    }

    pub fn insert(metric: NewMetric, conn: &PgConnection) -> Result<usize, Error> {
        diesel::insert_into(metrics::table)
            .values(&metric)
            .execute(conn)
    }

    pub fn from_uri(uri_: &str, conn: &PgConnection) -> Option<Metric> {
        use super::schema::metrics::dsl::*;
        let results = metrics.filter(uri.eq(uri_))
            .load::<Metric>(conn).ok()?;
        Some(results[0].clone())
    }

    pub fn inc_count(uri_: &str, conn: &PgConnection) -> Result<usize, Error> {
        use super::schema::metrics::dsl::*;

        let target = metrics.filter(uri.eq(uri_));
        diesel::update(target)
            .set(view_count.eq(view_count + 1))
            .execute(conn)
    }

    pub fn inc_count_or_insert(uri: &str, conn: &PgConnection) -> Result<usize, diesel::result::Error> {
        let mut num = Metric::inc_count(uri, &conn)?;

        if num == 0 {
            num = Metric::insert(NewMetric {
                uri: String::from(uri),
                view_count: 1
            }, &conn)?;
        }

        Ok(num)
    }
}
