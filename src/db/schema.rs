table! {
    metrics (id) {
        id -> Int4,
        uri -> Varchar,
        view_count -> Int4,
    }
}
