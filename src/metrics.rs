extern crate serde_json;

use rocket::Outcome;
use rocket::data::Data;
use rocket::fairing::{Fairing, Info, Kind};
use rocket::request::{Request, FromRequest};
use rocket_contrib::Template;

use diesel::prelude::*;

use db::DbConn;
use db::models::Metric;

#[derive(Serialize)]
struct MetricContext {
    error: Option<String>,
    metrics: Option<Vec<Metric>>
}

pub struct MetricCollector {}

impl MetricCollector {
    pub fn fairing() -> MetricCollector {
        MetricCollector {}
    }
}

impl Fairing for MetricCollector {
    fn info(&self) -> Info {
        Info {
            name: "Request Metric Collector",
            kind: Kind::Request
        }
    }

    fn on_request(&self, request: &mut Request, _: &Data) {
        let conn = match DbConn::from_request(&request) {
            Outcome::Success(c) => c,
            Outcome::Failure((e, _)) => {
                println!("Could not connect to database: {}", e);
                return
            },
            _ => {
                println!("Couldn't connect to database for unknown reason");
                return
            }
        };

        let uri_path = match request.uri().path().trim_right_matches('/') {
            "" => "/",
            s => s
        };

        match Metric::inc_count_or_insert(
            uri_path,
            &conn
        ) {
            Ok(_) => (),
            Err(e) => println!("Error incrementing count: {:?}", e)
        }
    }
}

#[get("/metrics")]
fn metrics_view(conn: DbConn) -> Template {
    use db::schema::metrics::dsl::*;

    let results = metrics.order((
        view_count.desc(),
        uri.asc()
    )).load::<Metric>(&*conn).ok();

    let context = MetricContext {
        error: None,
        metrics: results
    };

    Template::render("metrics", &context)
}
