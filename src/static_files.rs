extern crate rocket;

use std::path::{Path, PathBuf};

use rocket::response::NamedFile;

#[get("/static/<file..>")]
fn serve(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(file)).ok()
}
