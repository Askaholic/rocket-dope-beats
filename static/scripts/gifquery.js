var badgifs = [70, 80, 223, 244, 253];

function loadGif() {
	var num;
	do {
		num = (Math.floor(Math.random() * 327) + 1);
	} while(badgifs.indexOf(num) != -1);

	var url = "https://www.catgifpage.com/gifs/" + num + ".gif";
	var img = $("<div class='content'><img src='" + url + "' style='padding:30px 10px 0 15px' alt='loading...'></div><br/>");
	$("#container").append(img);
}
