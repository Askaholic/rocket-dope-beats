-- Your SQL goes here
CREATE TABLE metrics (
    id SERIAL PRIMARY KEY,
    uri VARCHAR NOT NULL UNIQUE,
    view_count INT NOT NULL DEFAULT 0
)
